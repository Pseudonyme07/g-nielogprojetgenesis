// Edit MAX from 50 to 200
#define MAX 200

typedef int tab_entiers[MAX];
 
// Fusion des listes t(de1..vers1) et t(de2..vers2) dans tmp(posInTmp..posInTmp+count-1)
void fusion(tab_entiers t, tab_entiers tmp, int de1, int vers1, int de2, int vers2, int count, int posInTmp) 
{
     int i;
     for(i = 0 ; i < count ; i++)
     {
              if (de2 > vers2)   // Si fin de la liste 2, on prend dans liste 1
                      tmp[posInTmp++] = t[de1++];
              else if (de1 > vers1)   // Idem si fin de la liste 1
                      tmp[posInTmp++] = t[de2++];
              else if (t[de1] <= t[de2])   // Enfin, sinon, on compare
                      tmp[posInTmp++] = t[de1++];
              else 
                      tmp[posInTmp++] = t[de2++];
      }
}
 
// Tri de tout le tableau t par fusions successives
void trifusion(tab_entiers t)
{
      tab_entiers tmp;
      int sortLength = 1, de1, de2, de3, i;
      while(sortLength < MAX)
      {
              de1 = 0;
              while(de1 < MAX)
              {
                    de2 = de1 + sortLength;
                    de3 = de2 + sortLength;
                    if(de2>MAX) de2 = MAX;
                    if(de3>MAX) de3 = MAX;
                    fusion(t, tmp, de1, de2-1, de2, de3-1, de3-de1, de1);
                    de1 = de3;
              }
              for(i = 0 ; i < MAX ; i++) t[i] = tmp[i];
              sortLength *= 2;
       }
 }
